<?php

namespace App\Http\ViewComposers;

use App\Repositories\NavMenuRepository;
use Illuminate\View\View;

class NavComposer
{
    protected $menu;

    public function __construct(NavMenuRepository $menu)
    {
        $this->menu = $menu;
    }

    public function compose(View $view)
    {
        $categories= $this->menu->all();

        $view->with([
            'categoryMenu' => $categories,
        ]);
    }
}