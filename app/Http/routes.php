<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => 'web'], function () {
    Route::auth();

    //FontpageController
    Route::get('/', 'FrontpageController@index');
    Route::get('/resources/{id}-{slug}', 'FrontpageController@showResource');
    Route::get('/categories/{id}-{slug}', 'FrontpageController@showCategory');
    Route::get('/search', 'FrontpageController@search');
    Route::get('/popular', 'FrontpageController@popular');

    //DashboardController
    Route::get('/dashboard', 'DashboardController@index');
    Route::get('/dashboard/settings', 'DashboardController@settings');
    Route::put('/dashboard/settings/{id}/changepassword', 'DashboardController@changePassword');

    //ResourcesController
    Route::get('/dashboard/resources/', 'ResourcesController@dashboard');
    Route::get('/dashboard/resources/create', 'ResourcesController@create');
    Route::post('/dashboard/resources/store', 'ResourcesController@store');
    Route::get('/dashboard/resources/{id}/edit', 'ResourcesController@edit');
    Route::put('/dashboard/resources/{id}/update', 'ResourcesController@update');
    Route::delete('/dashboard/resources/{id}/delete', 'ResourcesController@destroy');

    //CategoriesController
    Route::get('/dashboard/categories/', 'CategoriesController@dashboard');
    Route::get('/dashboard/categories/create', 'CategoriesController@create');
    Route::post('/dashboard/categories/store', 'CategoriesController@store');
    Route::get('/dashboard/categories/{id}/edit', 'CategoriesController@edit');
    Route::put('/dashboard/categories/{id}/update', 'CategoriesController@update');
    Route::delete('/dashboard/categories/{id}/delete', 'CategoriesController@destroy');
});
