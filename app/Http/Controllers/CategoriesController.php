<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Resource;
use App\Category;
use Session;
use Illuminate\Support\Facades\Input;

class CategoriesController extends Controller
{
    /**
* Create a new controller instance.
*
* @return void
*/
    public function __construct()
    {
        $this->middleware('auth');
    }
/**
* Show the application dashboard.
*
* @return \Illuminate\Http\Response
*/
    public function dashboard(){

        $categories = Category::Paginate(5);
        $pageTitle = 'Category';
        $pageDesc = 'welcome to categories page. See all categories here.';
        $no = 1;
        $titlePage = "Pixfuel - Categories Dashboard";
        return view('dashboard.categories', [
            'pageTitle' =>  $pageTitle,
            'pageDesc' =>  $pageDesc,
            'categories' => $categories,
            'no'    => $no,
            'titlePage'    => $titlePage,
            ]);
    }

    public function create(){

        $categories = Category::all();
        $pageTitle = 'Create Category';
        $pageDesc = 'welcome to create category page. Add new category here.';
        $titlePage = "Pixfuel - Create Dashboard";
        return view('dashboard.categoriesCreate', [
            'categories' => $categories, 
            'pageTitle' =>  $pageTitle,
            'pageDesc' =>  $pageDesc,
            'titlePage'    => $titlePage,
            ]);
    }

    public function store(Request $request) {
    	$this->validate($request, [
            'category_name' => 'required|max:255',
        ]);

        $category                   = new Category;
        $category->category_name    = $request->input('category_name');
        $category->slug             = str_slug($request->input('category_name'), "-");
        $category->save();

        Session::flash('flash_message', 'Category successfully added!');
        return redirect()->back();
    }

    public function edit($id) {
    	
        $category = Category::findOrFail($id);
        $pageTitle = 'Edit Category';
        $pageDesc = 'welcome to edit category page. Edit your category here.';
        $titlePage = "Pixfuel - Create Dashboard";
        return view('dashboard.categoriesEdit', [
            'category' => $category,
            'pageTitle' =>  $pageTitle,
            'pageDesc' =>  $pageDesc,
            'titlePage'    => $titlePage,
            ]);
    }

    public function update($id, Request $request) {

    	$this->validate($request, [
            'category_name' => 'required|max:255',
        ]);

        $category = Category::findOrFail($id);
        $category->category_name = str_slug($request->input('category_name'), "-");
    	$category->save();

        Session::flash('flash_message', 'Resource successfully updated!');
        return redirect()->back();

    }

    public function destroy($id){

    	$categoryCheck = Resource::where('category_id', '=' , $id )->count();

    	if($categoryCheck > 0){

         	Session::flash('error_message', 'Cant delete category , please make sure there is no resource related to this category.');
        	return redirect()->back();
    	}
    	else{
        	$category = Category::findOrFail($id);
        	$category->delete();

        	Session::flash('flash_message', 'Resource successfully Deleted!');
        	return redirect()->back();
    	}

        
    }
}
