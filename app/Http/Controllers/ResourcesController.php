<?php
namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Resource;
use App\Category;
use Session;
use File;



class ResourcesController extends Controller
{
/**
* Create a new controller instance.
*
* @return void
*/
    public function __construct()
    {
        $this->middleware('auth');
    }
/**
* Show the application dashboard.
*
* @return \Illuminate\Http\Response
*/


    public function dashboard()
    {
        $resources = Resource::Paginate(10);
        $pageTitle = 'Resources';
        $pageDesc = 'welcome to resources page. See all your resources here.';
        $titlePage = "Pixfuel - Resources Dashboard";
        $no = 1;
        return view('dashboard.resources', [
            'pageTitle' =>  $pageTitle,
            'pageDesc' =>  $pageDesc,
            'resources' => $resources,
            'no'    => $no,
            'titlePage'    => $titlePage,
            ]);
    }

    public function create()
    {
        $categories = Category::all();
        $pageTitle = 'Create Resource';
        $pageDesc = 'welcome to create resource page. Share your usefull resource.';
        $titlePage = "Pixfuel - Create Resource Dashboard";
        return view('dashboard.resourcesCreate', [
            'categories' => $categories, 
            'pageTitle' =>  $pageTitle,
            'pageDesc' =>  $pageDesc,
            'titlePage'    => $titlePage,
            ]);
    }

    public function store(Request $request) {

        $this->validate($request, [
            'resource_name' => 'required|max:255',
            'url_link' => 'required',
            'description' => 'required|max:255',
            'category_id' => 'required',
            'user_id' => 'required',
        ]);

        //$inputs = $request->all();
        //$resource = Resource::create($inputs);

        if ($request->file('image')->isValid()) {

                $destinationPath = 'uploads'; // upload path
                $originalFileName = $request->file('image')->getClientOriginalName();
                $onlyFileName = pathinfo($originalFileName, PATHINFO_FILENAME);
                $extension = $request->file('image')->getClientOriginalExtension(); // getting image extension
                $filenameSlug = str_slug($request->input('resource_name'), "_");
                $fileName = date('Y-m-d').'_'.$filenameSlug.'.'.$extension; // renameing image
                $request->file('image')->move($destinationPath, $fileName); // uploading file to given path
                
                $Resource                   = new Resource;
                $Resource->resource_name    = $request->input('resource_name');
                $Resource->url_link         = $request->input('url_link');
                $Resource->description      = $request->input('description');
                $Resource->category_id      = $request->input('category_id');
                $Resource->user_id          = $request->input('user_id');
                $Resource->img_filename     = $fileName;
                $Resource->slug             = str_slug($request->input('resource_name'), "-");
                $Resource->save();

                Session::flash('flash_message', 'Resource successfully added!');
                return redirect()->back();

            }

            else {
                // sending back with error message.
                Session::flash('error_message', 'uploaded file is not valid');
                return Redirect::to('upload');
        }

    }

    public function edit($id) {
        $resource = Resource::findOrFail($id);
        $categories = Category::all();
        $pageTitle = 'Edit Resources';
        $pageDesc = 'welcome to edit resource page. Edit your resources here.';
        $titlePage = "Pixfuel - Edit Resource Dashboard";
        return view('dashboard.resourcesEdit', [
            'resource' => $resource,
            'pageTitle' =>  $pageTitle,
            'pageDesc' =>  $pageDesc,
            'categories' =>  $categories,
            'titlePage'    => $titlePage,
            ]);
    }

    public function update($id, Request $request) {

        $resource = Resource::findOrFail($id);

        $this->validate($request, [
            'resource_name' => 'required|max:255',
            'url_link' => 'required',
            'description' => 'required|max:255',
            'category_id' => 'required',
            'user_id' => 'required',
        ]);

        
        $fileNameDB = $resource->img_filename;


        //$inputs = $request->all();
        //$resource = Resource::create($inputs);

        if ($request->hasFile('image')) {


                if ($request->file('image')->isValid()) {

                $destinationPath = 'uploads'; // upload path
                $originalFileName = $request->file('image')->getClientOriginalName();
                $onlyFileName = pathinfo($originalFileName, PATHINFO_FILENAME);
                $extension = $request->file('image')->getClientOriginalExtension(); // getting image extension
                $filenameSlug = str_slug($request->input('resource_name'), "_");
                $fileName = date('Y-m-d').'_'.$filenameSlug.'.'.$extension; // renameing image
                $request->file('image')->move($destinationPath, $fileName); // uploading file to given path
                
                
                $resource = Resource::findOrFail($id);

                $input = $request->except('image');
                $resource->fill($input)->save();
                $resource->img_filename = $fileName;
                $resource->slug         = str_slug($request->input('resource_name'), "-");
                $resource->save();

                Session::flash('flash_message', 'Resource successfully added!');
                return redirect()->back();

            }

            else {
                // sending back with error message.
                Session::flash('error', 'uploaded file is not valid');
                return Redirect::to('upload');
            }

        }

        else {

                $resource = Resource::findOrFail($id);
                $fileNameDB = $resource->img_filename;
                $fileName = $fileNameDB;

                $resource->resource_name    = $request->input('resource_name');
                $resource->url_link         = $request->input('url_link');
                $resource->description      = $request->input('description');
                $resource->category_id      = $request->input('category_id');
                $resource->user_id          = $request->input('user_id');
                $resource->slug             = str_slug($request->input('resource_name'), "-");
                $resource->img_filename     = $fileName;
                $resource->save();

                Session::flash('flash_message', 'Resource successfully updated!');
                return redirect()->back();

        }

    }

    public function destroy($id){

        $resource = Resource::findOrFail($id);
        $filename = 'uploads/'.$resource->img_filename;
        if (File::exists($filename)){
            File::delete($filename);
        }
        $resource->delete();

        Session::flash('flash_message', 'Resource successfully Deleted!');
        return redirect()->back();
    }
}