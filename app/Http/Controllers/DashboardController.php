<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Resource;
use App\Category;
use App\User;
use Session;
use Hash;

class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $totalResources = Resource::count();
        $totalCategories = Category::count();
        $totalUsers = User::count();
        $pageTitle = 'Dashboard';
        $pageDesc = 'welcome to dashboard page.';
        $titlePage = "Pixfuel - Dashboard";
        return view('dashboard.index', [
            'pageTitle' =>  $pageTitle,
            'pageDesc' =>  $pageDesc,
            'totalResources' => $totalResources,
            'totalCategories' => $totalCategories,
            'totalUsers' => $totalUsers,
            'titlePage' => $titlePage
            ]);
    }

    public function resources()
    {
        return view('dashboard.resources');
    }

    public function settings()
    {

        $pageTitle = 'Dashboard - Settings';
        $pageDesc = 'welcome to settings page.';
        $titlePage = "Pixfuel - Dashboard - Settings";
        return view('dashboard.settings', [
            'pageTitle' =>  $pageTitle,
            'pageDesc' =>  $pageDesc,
            'titlePage' => $titlePage
            ]);
    }

    public function changePassword($id, Request $request)
    {

        $user = User::findOrFail($id);
        $this->validate($request, [
            'confPassword' => 'required',
            'newPassword' => 'required',
            'confNewPassword' => 'required',
        ]);

        if (Hash::check($request->input('confPassword'), $user->password)) {
            if ($request->input('newPassword') == $request->input('confNewPassword')) {
                $user->password = bcrypt($request->input('newPassword'));
                $user->save();

                Session::flash('flash_message', 'Password has been changed.');
                return redirect()->back();
            }
            else {

                Session::flash('error_message', 'Password does not match the confirm password.');
                return redirect()->back();
            }
        }
        else {

                Session::flash('error_message', 'Your password is incorrect. Please try again!');
                return redirect()->back();
        }
    }

    



}
