<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use Nicolaslopezj\Searchable\SearchableTrait;
use App\Resource;
use App\Category;
use App\User;

use Carbon\Carbon;

class FrontpageController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $resources = Resource::orderBy('created_at','desc')->simplePaginate(15);
        $categories = Category::all();
        $titlePage = "Pixfuel - Free Graphics & Web Design Resources";
        return view('index', [
            'resources' =>  $resources,
            'categories' =>  $categories,
            'titlePage' =>  $titlePage,
            ]);
    }

    public function showResource($id)
    {

        $resource = Resource::findOrFail($id);
        $category_id = $resource->category_id;
        $resources = Resource::where('category_id', '=', $category_id)->orderByRaw("RAND()")->take(3)->get();
        $resources_without_currentId = $resources->except($id);
        $resource->increment('view_count');
        $resource->view_count + 1;
        $resource->save();
        $titlePage = "Pixfuel - ".$resource->resource_name;
        $metaImg = $resource->img_filename;
        $metaDesc = $resource->description;
        return view('resources', [
            'resource' =>  $resource,
            'resources' =>  $resources_without_currentId,
            'titlePage' =>  $titlePage,
            'metaImg' =>  $metaImg,
            'metaDesc' =>  $metaDesc
            ]);
    }

    public function showCategory($id)
    {

        $resources = Resource::where('category_id', '=', $id)->orderByRaw("RAND()")->simplePaginate(15);
        $category = Category::findOrFail($id);
        $titlePage = "Pixfuel - ".$category->category_name;
        return view('categories', [
            'resources' =>  $resources,
            'titlePage' =>  $titlePage,
            ]);
    }

    public function search(Request $request)
    {
        $keyword = $request->input('keyword');
        $resources = Resource::search($keyword)->simplePaginate(9);;
        $titlePage = "Pixfuel - Search :".$keyword;
        return view('search', [
            'resources' =>  $resources,
            'titlePage' =>  $titlePage,
            'keyword' =>  $keyword,
            ]);
    }

    public function popular()
    {

        $resources = Resource::orderBy('view_count','desc')->simplePaginate(15);
        $categories = Category::all();
        $titlePage = "Pixfuel - Popular Resources";
        return view('popular', [
            'resources' =>  $resources,
            'categories' =>  $categories,
            'titlePage' =>  $titlePage,
            ]);
    }

}
