<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Resource extends \Eloquent
{
    use \Nicolaslopezj\Searchable\SearchableTrait;
    protected $fillable = [
        'resource_name',
        'url_link',
        'category_id',
        'description',
        'user_id',
        'slug'
    ];

    protected $searchable = [
        'columns' => [
            'resource_name' => 10,
        ],
    ];

    public function category() {
        return $this->belongsTo('App\Category' , 'category_id');
    }
}
