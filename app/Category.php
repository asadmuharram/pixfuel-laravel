<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends \Eloquent
{
	protected $fillable = [
        'category_name',
    ];

    public function resource() {
        return $this->hasMany('App\Resource');
    }// categories
}
