<?php

namespace App\Repositories;

use App\Category;

class NavMenuRepository
{

    public function getCategoryMenu()
    {
        $categories = Category::all();
        return $categories;
    }

}