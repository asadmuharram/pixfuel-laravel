<?php

namespace App\Providers;
use View;
use App\Category;
use Illuminate\Support\ServiceProvider;

class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('layouts.app', function($view)
    {
        $categoriesMenu = Category::all();;
        $view->with('categoriesMenu', $categoriesMenu);
    });

    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /*private function composeNavigation()
    {
        view()->composer('*', 'App\Http\ViewComposers\NavComposer');
    }*/
}
