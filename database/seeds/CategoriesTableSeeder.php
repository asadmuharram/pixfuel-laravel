<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->delete();

        DB::table('categories')->insert([
            'category_name' => 'Web Design',
        	'created_at' => new DateTime,
            'updated_at' => new DateTime
        ]);
    }
}
