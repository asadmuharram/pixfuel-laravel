<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        DB::table('users')->insert([
            'name' => 'Muharram Asad',
        	'username' => 'muaramasad',
        	'email' => 'admin@pixfuel.com',
        	'password' => Hash::make('123456'),
        	'role_id' => '1',
        	'created_at' => new DateTime,
            'updated_at' => new DateTime
        ]);
    }
}
