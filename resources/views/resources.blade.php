@extends('layouts.app')
@section('content')
<div class="container-fluid">
    <div class="homepageContent">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                <!-- Display Validation Errors -->
                <div class="row">
                    <div class="titleResourceSingle">
                        <h3>{{$resource->resource_name}}</h3>
                        <em>submited {{ $resource->created_at->diffForHumans() }}</em>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-9">
                        <div class="panel panel-default">
                            <div class="panel-body cardPanelTable">
                                <img src="{{ asset('/') }}./uploads/{{$resource->img_filename}}" alt="" class="img-responsive imgResource">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <small>DESCRIPTION</small>
                        <p class="infoResouce">{{ $resource->description}}</p>
                        <nav class="sidebar-front">
                            <ul class="nav">
                                <li><a href="{{ url('dashboard/') }}">Views <span style="float:right">{{$resource->view_count}}</span></a></li>
                                <li><a href="{{ url('/categories/'.$resource->category->id.'-'.$resource->category->slug) }}">Category <span style="float: right;font-weight: normal;padding: 5px;background: #95a5a6" class="label label-default">{{$resource->category->category_name}}</span></a></li>
                                <li><a>Share <span style="float:right"><div class="addthis_sharing_toolbox"></div></span></a></li>
                                <li><a href="{{ $resource->url_link }}" target="blank" style="padding: 10px 0 0 0"><button type="button" class="btn btn-default btnLink" style=" width: 100%;
                                display: block;margin-top: 10px;background: #27ae60;color: #fff;border:1px solid #179964;padding: 10px 0">View Resources  <i class="fa fa-external-link"></i></button></a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="row">
                    <div class="titleResourceSingle">
                        <h3>Related Resources</h3>
                    </div>
                </div>
                <div class="row">
                    @foreach ($resources as $resource)
                    <div class="col-md-4">
                        <!-- Display Validation Errors -->
                        <div class="panel panel-default">
                            <div class="panel-body cardPanelTable">
                                <a href="{{ url('resources/'.$resource->id.'-'.$resource->slug) }}" class="imgWrap">
                                    <p class="resourceDesc">
                                        {{$resource->resource_name}}
                                    </p>
                                    <img src="{{ asset('/') }}./uploads/{{ $resource->img_filename}}" alt="" class="img-responsive imgResource">
                                </a>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection