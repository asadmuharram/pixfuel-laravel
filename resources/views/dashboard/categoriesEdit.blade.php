@extends('dashboard.dashboard')
@section('rightContent')
<div class="col-md-10">
    <!-- Display Validation Errors -->
    <div class="panel panel-default">
        <div class="panel-body cardPanelTable">
            @include('common.errors')
            @include('common.notifications')
            <form action="{{ url('dashboard/categories/'.$category->id.'/update/') }}" method="post" enctype="multipart/form-data">
                {!! csrf_field() !!}
                {{ method_field('PUT') }}
                <div class="form-group">
                    <label>Name</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="category_name" value="{{ $category->category_name}}">
                </div>
                <hr/>
                <button type="submit" class="btn btn-default">Submit</button>
                <a href="{{ URL::to('/dashboard/categories/') }}"><button type="button" class="btn btn-default">Cancel</button></a>
            </form>
        </div>
    </div>
</div>
@endsection