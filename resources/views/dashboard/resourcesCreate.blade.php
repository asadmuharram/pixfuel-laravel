@extends('dashboard.dashboard')
@section('rightContent')
<div class="col-md-10">
    <!-- Display Validation Errors -->
    <div class="panel panel-default">
        <div class="panel-body cardPanelTable">
            @include('common.errors')
            @include('common.notifications')
            <form action="{{ URL::to('/dashboard/resources/store') }}" method="post" enctype="multipart/form-data">
                {!! csrf_field() !!}
                <input type="hidden" class="form-control" id="exampleInputEmail1" value="{{ Auth::user()->id }}" placeholder="http://" name="user_id">
                <div class="form-group">
                    <label>Name</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="resource_name">
                </div>
                <div class="form-group">
                    <label>Url Link</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="http://" name="url_link">
                </div>
                <div class="form-group">
                    <label>Description</label>
                    <textarea class="form-control" rows="3" name="description"></textarea>
                </div>
                <div class="form-group">
                    <label for="exampleInputFile">File input</label>
                    <input type="file" id="exampleInputFile" name="image">
                    <p class="help-block">Make sure image format is jpg.</p>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Category</label>
                    <select class="form-control" name="category_id">
                        <option value="">Please Select Category</option>
                        @foreach ($categories as $category)
                        <option value="{{ $category->id }}">{{ $category->category_name }}</option>
                        @endforeach
                    </select>
                </div>
                <hr/>
                <button type="submit" class="btn btn-default">Submit</button>
            </form>
        </div>
    </div>
</div>
@endsection