@extends('dashboard.dashboard')
@section('rightContent')
<div class="col-md-10">
    <!-- Display Validation Errors -->
    <div class="panel panel-default">
        <div class="panel-body cardPanelTable">
            @include('common.errors')
            @include('common.notifications')
            <form action="{{ url('dashboard/resources/'.$resource->id.'/update/') }}" method="post" enctype="multipart/form-data">
                {!! csrf_field() !!}
                {{ method_field('PUT') }}
                <input type="hidden" class="form-control" id="exampleInputEmail1" value="{{ Auth::user()->id }}" placeholder="http://" name="user_id">
                <div class="form-group">
                    <label>Name</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="" name="resource_name" value="{{ $resource->resource_name}}">
                </div>
                <div class="form-group">
                    <label>Url Link</label>
                    <input type="text" class="form-control" id="exampleInputEmail1" placeholder="http://" name="url_link" value="{{ $resource->url_link}}">
                </div>
                <div class="form-group">
                    <label>Description</label>
                    <textarea class="form-control" rows="3" name="description">{{ $resource->description }}</textarea>
                </div>
                <div class="form-group">
                    <label>Image</label>
                    <img src="{{ asset('/') }}./uploads/{{ $resource->img_filename}}" alt="" class="img-responsive">
                </div>
                <div class="form-group">
                    <label for="exampleInputFile">File input</label>
                    <input type="file" id="exampleInputFile" name="image">
                    <p class="help-block">Make sure image format is jpg.</p>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Category</label>
                    <select class="form-control" name="category_id">
                        <option value="">Please Select Category</option>
                        @foreach ($categories as $category)
                        @if ($resource->category_id == $category->id)
                        <option selected value="{{ $category->id }}">{{ $category->category_name }}</option>
                        @else
                        <option value="{{ $category->id }}">{{ $category->category_name }}</option>
                        @endif
                        @endforeach
                    </select>
                </div>
                <hr/>
                <button type="submit" class="btn btn-default">Submit</button>
                <a href="{{ URL::to('/dashboard/resources/') }}"><button type="button" class="btn btn-default">Cancel</button></a>
            </form>
        </div>
    </div>
</div>
@endsection