@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row topSection">
        <div class="col-md-6 pull-left">
            <h3>{{ $pageTitle }}</h3>
            <small>{{ $pageDesc }}</small>
        </div>
        @if( Request::is('dashboard/resources') ? 'active' : '')
        <div class="col-md-6 pull-right rightside">
            <a class="btn btn-primary" href="{{ url('dashboard/resources/create') }}" role="button"><i class="fa fa-plus"></i> New Resources</a>
        </div>
        @elseif( Request::is('dashboard/categories') ? 'active' : '')
        <div class="col-md-6 pull-right rightside">
            <a class="btn btn-primary" href="{{ url('dashboard/categories/create') }}" role="button"><i class="fa fa-plus"></i> New Category</a>
        </div>
        @endif

    </div>
    <br />
    
    <div class="row">
        <div class="col-md-2">
            <nav class="nav-sidebar">
                <ul class="nav">
                    <li><a href="{{ url('dashboard/') }}"><i class="fa fa-btn fa-list-alt"></i>Dashboard</a></li>
                    <li><a href="{{ url('dashboard/resources/') }}"><i class="fa fa-file-image-o"></i>  Resources</a></li>
                    <li><a href="{{ url('dashboard/categories/') }}"><i class="fa fa-tasks"></i>  Categories</a></li>
                    <!--<li><a href="{{ url('dashboard/users/') }}"><i class="fa fa-users"></i>  Users</a></li>
                    <li><a href="{{ url('dashboard/roles/') }}"><i class="fa fa-gavel"></i>  Role</a></li>-->
                    <li><a href="{{ url('dashboard/settings/') }}"><i class="fa fa-cog"></i>  Settings</a></li>
                </ul>
            </nav>
        </div>
        @yield('rightContent')
    </div>
</div>
@endsection

