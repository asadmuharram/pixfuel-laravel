@extends('dashboard.dashboard')
@section('rightContent')
    <div class="col-md-4">
            <div class="panel panel-default">
                <div class="panel-heading">Total All Resources</div>
                <div class="panel-body cardPanel">
                    <h2>{{$totalResources}}</h2>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading">Total All Categories</div>
                <div class="panel-body cardPanel">
                    <h2>{{$totalCategories}}</h2>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="panel panel-default">
                <div class="panel-heading">Total All Users</div>
                <div class="panel-body cardPanel">
                    <h2>{{$totalUsers}}</h2>
                </div>
            </div>
        </div>
@endsection