@extends('dashboard.dashboard')
@section('rightContent')
<div class="col-md-10">
    <div class="panel panel-default">
        <div class="panel-body cardPanelTable">
            @include('common.errors')
            @include('common.notifications')
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($categories as $category)
                    <tr>
                        <th scope="row">{{$categories->perPage()*($categories->currentPage()-1)+$no}}</th>
                        <td>{{ $category->category_name }}</td>
                        <td>
                            <a href="{{ url('/categories/'.$category->id) }}" class="btn btn-success btn-xs"><i class="fa fa-external-link"></i> View</a>
                            <a href="{{ url('dashboard/categories/'.$category->id.'/edit/') }}" class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o"></i> Edit</a>
                            <form action="{{ url('dashboard/categories/'.$category->id.'/delete') }}" method="POST" style="display: inline">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <button type="submit" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete</button>
                            </form>
                        </td>
                </tr>
                    <?php
                        $no++;
                    ?>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
{!! $categories->render() !!}
</div>
@endsection