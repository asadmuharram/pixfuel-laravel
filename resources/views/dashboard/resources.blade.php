@extends('dashboard.dashboard')
@section('rightContent')
<div class="col-md-10">
    <div class="panel panel-default">
        <div class="panel-body cardPanelTable">
            @include('common.errors')
            @include('common.notifications')
            <table class="table table-striped table-hover">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>Category</th>
                        <th>Url Link</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($resources as $resource)
                    <tr>
                        <th scope="row">{{$resources->perPage()*($resources->currentPage()-1)+$no}}</th>
                        <td>{{ $resource->resource_name }}</td>
                        <td>{{ $resource->category->category_name }}</td>
                        <td><a href="{{ $resource->url_link }}" target="blank">
                            Visit Link
                        </a>
                    </td>
                    <td>
                        <a href="{{ url('resources/'.$resource->id.'-'.$resource->slug) }}" class="btn btn-success btn-xs" target="blank"><i class="fa fa-external-link"></i> View</a>
                        <a href="{{ url('dashboard/resources/'.$resource->id.'/edit/') }}" class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o"></i> Edit</a>
                        <form action="{{ url('dashboard/resources/'.$resource->id).'/delete' }}" method="POST" style="display: inline">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}
                            <button type="submit" class="btn btn-danger btn-xs"><i class="fa fa-trash-o"></i> Delete</button>
                        </form>
                    </td>
                </tr>
                    <?php
                        $no++;
                    ?>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
            {!! $resources->render() !!}
            </div>
        </div>
</div>
@endsection