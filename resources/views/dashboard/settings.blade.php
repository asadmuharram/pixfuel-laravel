@extends('dashboard.dashboard')
@section('rightContent')
<div class="col-md-10">
    <!-- Display Validation Errors -->
    <div class="panel panel-default">
        <div class="panel-body cardPanelTable">
             <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="#passSet" aria-controls="passRes" role="tab" data-toggle="tab">Password Setting</a></li>
                <li role="presentation"><a href="#emailSet" aria-controls="emailSet" role="tab" data-toggle="tab">Email Setting</a></li>
            </ul>
            <!-- Tab panes -->
            <div class="tab-content">
                <div role="tabpanel" class="tab-pane active" id="passRes">
                    <h3>Change Password</h3>
                    <br/>
                    @include('common.errors')
                    @include('common.notifications')
                    <form action="{{ url('dashboard/settings/'.Auth::user()->id.'/changepassword/') }}" method="post" enctype="multipart/form-data">
                        {!! csrf_field() !!}
                        {{ method_field('PUT') }}
                        <input type="hidden" class="form-control" id="exampleInputEmail1" value="{{ Auth::user()->id }}" name="user_id">
                        <div class="form-group">
                            <label>Confirm Existing Password</label>
                            <input type="password" class="form-control" name="confPassword">
                        </div>
                        <hr/>
                        <div class="form-group">
                            <label>New Password</label>
                            <input type="password" class="form-control" name="newPassword">
                        </div>
                        <div class="form-group">
                            <label>Confirm New Password</label>
                            <input type="password" class="form-control" name="confNewPassword">
                        </div>
                        <hr/>
                        <button type="submit" class="btn btn-default">Submit</button>
                        <a href="{{ URL::to('/dashboard/') }}"><button type="button" class="btn btn-default">Cancel</button></a>
                    </form>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection