@extends('layouts.app')
@section('content')
<div class="container-fluid">
    <div class="homepageContent">
        @foreach($resources->chunk(3) as $items)
        <div class="row">
            @foreach($items as $resource)
            <div class="col-md-4">
                <!-- Display Validation Errors -->
                <div class="panel panel-default">
                    <div class="panel-body cardPanelTable">
                        <a href="{{ url('resources/'.$resource->id.'-'.$resource->slug) }}" class="imgWrap">
                            <p class="resourceDesc">{{ $resource->description }}</p>
                            <img src="{{ asset('/') }}./uploads/{{ $resource->img_filename}}" alt="" class="img-responsive imgResource">
                            <div class="infoResources">
                                <p class="titleResource">{{$resource->resource_name}}</p>
                                <ul class="infoGroup">
                                    <li class="dateSubmited">
                                        Submited {{ $resource->created_at->diffForHumans() }}
                                    </li>
                                </ul>
                            </div>
                            
                        </a>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        @endforeach
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
            {!! $resources->render() !!}
            </div>
        </div>
    </div>
</div>
@endsection