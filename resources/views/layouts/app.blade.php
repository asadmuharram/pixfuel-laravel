<?php
$metaDescDefault = "Best Curated Graphics, Website Design, UIUX, Prints, Icons Resources";
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{{$titlePage}}</title>
        <meta property="og:title" content="{{$titlePage}}"/>
        <meta property="og:site_name" content="Pixfuel"/>
        @if (Request::is('/*'))
        <meta property="og:image" content="{{ URL::asset('assets/images/pixfuelMeta.jpg') }}"/>
        @elseif (ends_with(Route::currentRouteAction(), 'FrontpageController@showResource'))
        <meta property="og:image" content="{{ asset('/') }}uploads/{{$metaImg}}"/>
        @else
        <meta property="og:image" content="{{ URL::asset('assets/images/pixfuelMeta.jpg') }}"/>
        @endif
        @if (ends_with(Route::currentRouteAction(), 'FrontpageController@showResource'))
        <meta property="og:description" content="{{$metaDesc}}"/>
        @else
        <meta property="og:description" content="{{$metaDescDefault}}"/>
        @endif
        <meta name="twitter:card" content="summary_large_image">
        <meta name="twitter:site" content="www.pixfuel.com">
        <meta name="twitter:creator" content="@pixfuel_">
        <meta name="twitter:title" content="{{$titlePage}}">
        @if (ends_with(Route::currentRouteAction(), 'FrontpageController@showResource'))
        <meta property="twitter:description" content="{{$metaDesc}}"/>
        @else
        <meta property="twitter:description" content="{{$metaDescDefault}}"/>
        @endif
        @if (Request::is('/*'))
        <meta name="twitter:image" content="{{ URL::asset('assets/images/pixfuelMeta.jpg') }}"/>
        @elseif (ends_with(Route::currentRouteAction(), 'FrontpageController@showResource'))
        <meta name="twitter:image" content="{{ asset('/') }}uploads/{{$metaImg}}"/>
        @else
        <meta name="twitter:image" content="{{ URL::asset('assets/images/pixfuelMeta.jpg') }}"/>
        @endif
        <meta property="fb:admins" content="1223603088" />
        <link rel="shortcut icon" href="{{ URL::asset('assets/images/favicon.ico') }}">
        <!-- Fonts -->
        <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.4.0/css/font-awesome.min.css" rel='stylesheet' type='text/css'>
        <!-- Styles -->
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="{{ URL::asset('assets/css/style.css') }}" />
        <link rel="stylesheet" href="{{ URL::asset('assets/fonts/font.css') }}" />
        <link rel="stylesheet" href="{{ URL::asset('assets/css/animate.css') }}" />
        {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
        <script src="{{ URL::asset('assets/js/flowtype.js') }}"></script>
        <script type="text/javascript">
        jQuery("#wrapper").fitText();
        </script>
    </head>
    <body>
        <div id="wrapper">
            <div id="content">
                <nav class="navbar navbar-default whiteNav">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <!-- Collapsed Hamburger -->
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse">
                            <span class="sr-only">Toggle Navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            </button>
                            <!-- Branding Image -->
                            <a class="navbar-brand" href="{{ url('/') }}">
                                
                            </a>
                        </div>
                        <div class="collapse navbar-collapse" id="app-navbar-collapse">
                            <!-- Left Side Of Navbar-->
                            <ul class="nav navbar-nav">
                                @foreach ($categoriesMenu as $category)
                                <li><a href="{{ url('/categories/'.$category->id.'-'.$category->slug) }}">{{$category->category_name}}</a></li>
                                @endforeach
                            </ul>
                            <ul class="nav navbar-nav navbar-right">
                                <!-- Authentication Links -->
                                @if (Auth::guest())
                                
                                @elseif (Auth::user())
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                        {{ Auth::user()->name }} <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="{{ url('/dashboard') }}"><i class="fa fa-btn fa-list-alt"></i>Dashboard</a></li>
                                        <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                                    </ul>
                                </li>
                                @else
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                        {{ Auth::user()->name }} <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                                    </ul>
                                </li>
                                @endif
                            </ul>
                            <form role="search" class="navbar-form navbar-right" action="{{ URL::to('/search/') }}" method="get">
                                <div class="input-group">
                                    <input type="text" class="form-control" placeholder="search" name="keyword">
                                    <span class="input-group-btn">
                                        <button type="submit" class="btn btn-default">
                                        <span class="glyphicon glyphicon-search"></span>
                                        </button>
                                    </span>
                                </div>
                            </form>
                            <!-- Right Side Of Navbar -->
                        </div>
                    </div>
                </nav>
                @yield('content')
            </div>
            <div id="footer">
                <div class="container-fluid">
                    <p class="text-muted"><span class="pixfuelFooter">© 2016 <strong>Pixfuel</strong> - All resources are copyright of their owners. We only link to the resources & we don't own any of them.</span>
                    <!--<span class="infoFooter">
                        <a href="https://www.facebook.com/PixfuelMedia/"><i class="fa fa-facebook-square fa-lg"></i></a>
                        <a href="https://twitter.com/pixfuel_"><i class="fa fa-twitter-square fa-lg"></i></a>
                    </span>--></p>  
                </div>
            </div>
        </div>
        <!-- JavaScripts -->
        {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-52a9efba2e5fa033"></script>
        <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
        ga('create', 'UA-46400546-1', 'auto');
        ga('send', 'pageview');
        </script>
        <script type="text/javascript">
        $(document).ready(function () {
        window.setTimeout(function() {
        $(".alert").fadeTo(1000, 0).slideUp(400, function(){
        $(this).remove();
        });
        }, 3000);
        });
        </script>
    </body>
</html>